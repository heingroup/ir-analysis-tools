import os
import platform
from typing import Optional

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from numpy import average, diff
from scipy.integrate import quad
from scipy.interpolate import UnivariateSpline


def reject_outliers(data, m=2.):
    d = np.abs(data - np.median(data))
    mdev = np.median(d)
    s = d / mdev if mdev else 0.
    return data[s < m]


def creation_date(path_to_file):
    """
    Try to get the date that a file was created, falling back to when it was
    last modified if that isn't possible.
    See http://stackoverflow.com/a/39501288/1709587 for explanation.
    """
    if platform.system() == 'Windows':
        return os.path.getctime(path_to_file)
    else:
        stat = os.stat(path_to_file)
        try:
            return stat.st_birthtime
        except AttributeError:
            # We're probably on Linux. No easy way to get creation dates here,
            # so we'll settle for when its content was last modified.
            return stat.st_mtime


class IRAnalysisTools:

    def __init__(self, data_directory, analyze_until=None, targets: Optional[dict] = None,
                 ir_threshold_ratio : Optional[float] = 0.002):
        """
        read and store data in the directory
        :param data_directory: path of the directory that contains csv files
        """
        self.PLATEAU_THRESHOLD_PERCENTAGE = 1
        # common substances for range integration
        # value format: [from, to]
        self.COMMON_RANGE = {
            'CDI': [1380, 1410],
            'CO2': [2327, 2350]
        }
        if targets is not None:
            if type(targets) is dict:
                self.COMMON_RANGE.update(targets)
            else:
                raise TypeError('The input for targets is not valid.')
        self.IR_THRESHOLD_RATIO = ir_threshold_ratio
        self.data_directory = data_directory
        self.absorbance = None
        self.wavenumber = None
        self.integration_range = None
        self.time = None
        self.load(analyze_until=analyze_until)

    def load(self, analyze_until=None):
        """
        load the data into the variables for further analysis
        """
        files = os.listdir(self.data_directory)
        # sort base on time
        files.sort(key=lambda x: os.path.getmtime(os.path.join(self.data_directory, x)))
        self.absorbance = [np.array(pd.read_csv(os.path.join(self.data_directory, file)).iloc[:, 1]) for file in files]
        self.wavenumber = [np.array(pd.read_csv(os.path.join(self.data_directory, file)).iloc[:, 0]) for file in files]
        self.time = [creation_date(os.path.join(self.data_directory, file)) for file in files]
        self.time = [(self.time[i] - self.time[0]) / 3600 for i in range(len(self.time))]  # in hour

        if analyze_until is not None:
            try:
                self.absorbance = self.absorbance[:analyze_until]
                self.wavenumber = self.wavenumber[:analyze_until]
                self.time = self.time[:analyze_until]
            except IndexError:
                pass

    def integration(self, x, y, degree: int = 5):
        # interpolation
        fn = UnivariateSpline(x, y, k=degree)
        results = quad(fn, self.integration_range[0], self.integration_range[1])[0]
        return results

    def get_latest_integration(self, target: str or list, reload: bool = True):
        """
        get the integration of the substance from the last trial
        :param reload: if reload the data
        :param target: Enum[keys in self.COMMON_RANGE] or [from wavenumber, to wavenumber]
        :return: the integration of that ranger (type: float)
        """
        target = self.pre_configuration(reload, target)

        result = self.get_an_integration(target, -1)

        return result

    def get_an_integration(self, target: str or list, index: int):
        """
        get one region of integration
        :param target: Enum[keys in self.COMMON_RANGE] or [from wavenumber, to wavenumber]
        :param index: The position of the data that is integrated
        :return:
        """
        # create variables for range of integration
        integrate_from = target[0]
        integrate_to = target[1]
        # get the last data
        absorbance = self.absorbance[index]
        wavenumber = self.wavenumber[index]
        # get the indices that are in the range
        target_indices = (np.intersect1d(np.where(integrate_from <= wavenumber),
                                         np.where(integrate_to >= wavenumber)))
        # update only the indices to reduce approximation
        absorbance = absorbance[min(target_indices) - 1: max(target_indices) + 1]
        wavenumber = wavenumber[min(target_indices) - 1: max(target_indices) + 1]
        # normally the values are in decreasing order. handle all cases
        try:
            result = self.integration(wavenumber, absorbance)
        except ValueError:
            result = self.integration(np.flip(wavenumber), np.flip(absorbance))
        return result

    def pre_configuration(self, reload, target):
        if reload:
            self.load()
        # check if it is a common key
        if type(target) == str and self.COMMON_RANGE.keys().__contains__(target):
            target = self.COMMON_RANGE[target]
        # check if the input is valid
        if type(target) is not list:
            raise ValueError('Input target is not valid.')
        return target

    def get_time_course_integration(self, target: str or list, reload: bool = True, plot: bool = False):
        """
        get the integration of the substance from the last trial
        :param plot: if we want to plot
        :param reload: if reload the data
        :param target: Enum[keys in self.COMMON_RANGE] or [from wavenumber, to wavenumber]
        :return: the integration of that ranger (type: float)
        """
        target = self.pre_configuration(reload, target)
        self.integration_range = target
        results = []
        for i in range(len(self.wavenumber)):
            results.append(self.get_an_integration(target, i))
        if plot:
            plt.plot(self.time, results)
            plt.title(str(target))
            plt.xlabel('Time (hour)')
            plt.ylabel('Height (A.U.)')
            plt.show()

        return results

    def get_time_course_height(self, target: str or list, reload: bool = True, plot: bool = False):
        """
        get the height of the substance from the last trial
        :param plot: if we want to plot
        :param reload: if reload the data
        :param target: Enum[keys in self.COMMON_RANGE] or [from wavenumber, to wavenumber]
        :return: the integration of that ranger (type: float)
        """
        target = self.pre_configuration(reload, target)
        self.height = average(target)

        index_left = max((np.where(np.array(self.wavenumber[0]) > target[1]))[0])
        index_right = max((np.where(np.array(self.wavenumber[0]) > target[0]))[0])
        results = []
        index = max((np.where(np.array(self.wavenumber[0]) > self.height))[0])
        for i in range(len(self.wavenumber)):
            base_line = (self.absorbance[i][index_left]+self.absorbance[i][index_right])/2

            results.append(self.absorbance[i][index]-base_line)

        if plot:
            plt.plot(self.time, results)
            plt.title(str(target))
            plt.xlabel('Time (hour)')
            plt.ylabel('Height (A.U.)')
            plt.show()

        return results

    def is_plateau(self,
                   target: str or list,
                   reload: bool = True,
                   amount_of_interest: int = 10,
                   evaluate_method: str = 'height'):
        """
        determine if the current data set reaches a endpoint
        :param target: Enum[keys in self.COMMON_RANGE] or [from wavenumber, to wavenumber]
        :param reload: if reload the data
        :param amount_of_interest: The amount of data points we are looking back
        :return: if the state is in plateau
        """
        if evaluate_method == 'height':
            progress = self.get_time_course_height(target, reload)
        elif evaluate_method == 'integration':
            progress = self.get_time_course_integration(target, reload)
        else:
            raise ValueError('Input for evaluate_method is not valid.')
        # largest_area = max(progress)
        # threshold = largest_area * self.PLATEAU_THRESHOLD_PERCENTAGE / 100
        # progress_of_interest = progress[-amount_of_interest:]
        diff_in_time = diff(self.time)[0] * amount_of_interest
        diff_in_y = max(
            progress
        [-amount_of_interest:]) - min(
            progress[-amount_of_interest:])
        difference = diff_in_y / diff_in_time
        print(difference)

        if abs(difference) < self.IR_THRESHOLD_RATIO:
            return True
        # for poi in progress_of_interest:
        #     if poi < threshold:
        #         return True

        return False

    def get_progress(self,
                   target: str or list,
                   reload: bool = True,
                   amount_of_interest: int = 10,
                   is_low: bool = True,
                   evaluate_method: str = 'height'):
        """
        determine if the current data set reaches a endpoint
        :param is_low: whether this is a low point or a high point
        :param target: Enum[keys in self.COMMON_RANGE] or [from wavenumber, to wavenumber]
        :param reload: if reload the data
        :param amount_of_interest: The amount of data points we are looking back
        :return: if the state is in plateau
        """
        if evaluate_method == 'height':
            progress = self.get_time_course_height(target, reload)
        else:
            progress = self.get_time_course_integration(target, reload)
        largest_area = max(progress)
        threshold = largest_area * self.PLATEAU_THRESHOLD_PERCENTAGE / 100
        progress_of_interest = progress[-amount_of_interest:]
        # return false if is not flat or above threshold
            # if is_low and poi > threshold:
        diff_in_time = diff(self.time)[0] * amount_of_interest
        diff_in_y = max(
            progress
        [-amount_of_interest:]) - min(
            progress[-amount_of_interest:])
        difference = diff_in_y / diff_in_time
        progress = abs(difference - 0.036)/0.034
        if progress > 1:
            return 1
        if progress < 0:
            return 0
        else:
            return progress


if __name__ == '__main__':
    ir = IRAnalysisTools('/Users/luke/PycharmProjects/hydra/experiments/utilities/hplc_analysis_tools/test/iC IR Experiments/hydra-069 Exp 2021-12-09 14-06')
    ir.get_time_course_height(target='CDI', plot=True)